(ns user
  (:require 
            [mount.core :as mount]
            [fuber.core :refer [start-app]]))

(defn start []
  (mount/start-without #'fuber.core/repl-server))

(defn stop []
  (mount/stop-except #'fuber.core/repl-server))

(defn restart []
  (stop)
  (start))


