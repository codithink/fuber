(ns fuber.routes.route-utils
  (:require [ring.util.http-response :refer [content-type ok]]
            [fuber.layout :refer [render-json]]))

(defn send-error-response
  "Utility function to send error message"
  [error-message]
  (render-json {:status false
                :error {:message (or error-message
                                     "Sorry! something went wrong.")}}))

(defn send-success-response
  "Utility function to send response data,
   fallbacks to empty map if data is nil"
  [data]
  (render-json {:status true
                :data (or data {})}))
