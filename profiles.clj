{:profiles/common    {:env {:database {:host "127.0.0.1"
                                       :port 27017}
                            :options {:port 3000
                                      :nrepl-port 7000}}}
 :profiles/dev       {:env {:database {:db-name "fuber"}
                            :profile "dev"}}
 :profiles/test      {:env {:database {:db-name "fuber-test"}
                            :profile "test"
                            :options {:port 3001
                                      :nrepl-port 7001}}}}
