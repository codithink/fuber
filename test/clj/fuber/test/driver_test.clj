(ns fuber.test.driver-test
  (:use midje.sweet)
  (:require [midje.config :refer :all]
            [fuber.driver :refer :all]
            [fuber.test.utils :as u]
            [clojure.core.async :refer [<! go timeout]]))

(change-defaults :print-level :print-facts)

(against-background
 [(before :contents (u/setup))
  (after :contents (u/teardown))]

 (facts "get-nearest-driver function"

        (fact "whether the driver returned is in proper format"
              (get-nearest-driver [20.0 19.0] false)
              => (fn [res]
                   (map? res)))

        (fact "whether only available driver is returned"
              (get-nearest-driver [18.0 5.0] false)
              => (fn [res]
                   (= (:available? res))))

        (fact "that no driver gets returned if the customer is too far away"
              (get-nearest-driver [120.0 105.0] false)
              => (fn [res]
                   (= "No cars nearby" res)))

        (fact "Pink drivers only should be returned if requested for pink drivers"
              (get-nearest-driver [22.4 19.1] true)
              => (fn [res]
                   (:pink? res)))

        (fact "Pink drivers should not be returned for normal customers"
              (get-nearest-driver [23.0 26.1] false)
              => (fn [res]
                   (false? (:pink? res)))))

 (facts "Start car trip function"

        (fact "Occupied car should not be available"

              (start-car-trip (:id (:occupied-car @u/test-data))
                              (:id (:available-customer @u/test-data))
                              [14.5 14.5]
                              [19.0 19.0])
              => (fn [res]
                   (and (false? (:trip-started? res))
                        (= "Car is already occupied"
                           (:error-message res)))))

        (fact "Non existent car should be denied"

              (start-car-trip (:id (:non-existent-car @u/test-data))
                              (:id (:available-customer @u/test-data))
                              [14.5 14.5]
                              [19.0 19.0])
              => (fn [res]
                   (and (= "No such car exists." (:error-message res))
                        (false? (:trip-started? res)))))

        (fact "Available car should be available for trip and should contain the relevant keys"

              (start-car-trip (:id (:available-car @u/test-data))
                              (:id (:available-customer @u/test-data))
                              [14.5 14.5]
                              [19.0 19.0])
              => (fn [res]
                   (and (:trip-started? res)
                        (= (into #{} (keys res))
                           #{:trip-started? :source
                             :destination :trip-id
                             :estimate-distance
                             :estimate-time-mins
                             :estimate-fare})))

              (start-car-trip (:id (:available-pink-car @u/test-data))
                              (:id (:available-customer-2 @u/test-data))
                              [14.5 14.5]
                              [19.0 19.0])
              => (fn [res]
                   (and (:trip-started? res)
                        (= (into #{} (keys res))
                           #{:trip-started? :source
                             :destination :trip-id
                             :estimate-distance
                             :estimate-time-mins
                             :estimate-fare}))))

        (fact "A booked car should not be for any other customer(Concurrency check)"

              (apply pcalls (mapv (fn [_]
                                    #(start-car-trip (:id (:available-car-5 @u/test-data))
                                                    (:id (:available-customer-5 @u/test-data))
                                                    [24.0 15.0]
                                                    [27.0 27.0]))
                                  (range 200)))

              => (fn [res]
                   (and (= (count (filterv :trip-started? res))
                           1)
                        (= (count (filterv #(not (:trip-started? %)) res))
                           199))))


        (fact "Customer who is already on trip cannot book another cab"
              (start-car-trip (:id (:available-car-4 @u/test-data))
                              (:id (:on-trip-customer @u/test-data))
                              [14.5 14.5]
                              [19.0 19.0])
              => (fn [res]
                   (and (false? (:trip-started? res))
                        (= "A customer can book only one trip at a time"
                           (:error-message res)))))

        (fact "Check if the estimated-trip values conform to the formula"

              (start-car-trip (:id (:available-car-3 @u/test-data))
                              (:id (:available-customer-3 @u/test-data))
                              [14.5 14.5]
                              [19.0 19.0])
              => (fn [res]
                   (and (= (:estimate-time-mins res) 19)
                        (= (:estimate-fare res) 31.72)
                        (= (:estimate-distance res) 6.36)))

              (start-car-trip (:id (:available-pink-car-2 @u/test-data))
                              (:id (:available-customer-4 @u/test-data))
                              [14.5 14.5] [19.0 19.0])
              => (fn [res]
                   (and (= (:estimate-time-mins res) 19)
                        (= (:estimate-fare res) 36.72)
                        (= (:estimate-distance res) 6.36)))))

  ;; Update trip started-time to 1.5 mins ago to
  (u/load-trips-data)


  (->> [:normal-trip :pink-trip]
       (map (fn [k]
              (:id (k @u/test-data))))
       (run! u/change-source-time))

 (facts "End trip function"


          (fact "Throw error if trip-id does not exist in the database"

                (end-car-trip (:invalid-trip-id @u/test-data)
                              [14.5 14.5])
              => (fn [res]
                   (and (false? (:trip-finished? res))
                        (= (:error-message res)
                           "Trip-id does not exist in the database."))))

          (fact "Car which is on trip should end the trip and Fare calculated for a normal car should be correct according to the formula"

              (end-car-trip (:id (:normal-trip @u/test-data))
                            [23.0 25.0])
              => (fn [res]
                   (and (:trip-finished? res)
                        (= (:fare res) 28.02))))


        (fact "if finished trip cannot be updated"

                (end-car-trip (:id (:normal-trip @u/test-data))
                              [23.5 22.5])
                => (fn [res]
                     (and (false? (:trip-finished? res))
                          (= (:error-message res)
                             "The trip has already finished."))))

        (fact "Fare calculated for a pink car should be 5 dodgecoins more than a normal car"
                (end-car-trip (:id (:pink-trip @u/test-data))
                              [23.0 25.0])
              => (fn [res]
                   (and (:trip-finished? res) (= (:fare res) 33.02)))))

 (facts "Booking history functions"

        (fact "If all the trips are in the correct format"

              (booking-history (:id (:available-car @u/test-data))
                               "driver")
              => (fn [res]
                   (every? (fn [trip]
                             (= #{:id :cust-id :driver-id
                                  :fare :started-time :end-time
                                  :source :destination :on-trip?
                                  :distance :pink?}
                                (into #{} (keys trip))))
                           res))

              (booking-history (:id (:available-customer @u/test-data))
                               "customer")
              => (fn [res]
                   (every? (fn [trip]
                             (= #{:id :cust-id :driver-id
                                  :fare :started-time :end-time
                                  :source :destination :pink?
                                  :distance :on-trip?}
                                (into #{} (keys trip))))
                           res)))

        (fact "if only the completed trips are returned"

              (booking-history (:id (:available-car @u/test-data))
                               "driver")

              => (fn [res]
                   (every? (fn [trip]
                             (false? (:on-trip? trip)))
                           res))

              (booking-history (:id (:available-customer @u/test-data))
                               "customer")

              => (fn [res]
                   (every? (fn [trip]
                             (false? (:on-trip? trip)))
                           res)))

        (fact "if it accepts value of who other than driver and customer"

              (booking-history (:id (:available-car @u/test-data))
                               "driver1232")

              => (throws AssertionError))

        (fact "if id is nil, error should be thrown"

              (booking-history nil
                               "customer")

              => (throws AssertionError)))


 (facts "calculate fare function tests"

        (fact "if fare for 1 km and 1 minute is 3 dodgecoins"

                (calculate-fare 1.0 1 false)
                => 3.0)

        (fact "if fare for 1 km and 1 minute for PINK cars it is 5 dodgecoins more than normal cars"

              (calculate-fare 1.0 1 true) => 8.0)

        (fact "if assertion error is thrown for invalid input"

              (calculate-fare 12 4 true) => (throws AssertionError)

              (calculate-fare -12.0 4 true) => (throws AssertionError)

              (calculate-fare 12.0 -4 true) => (throws AssertionError)

              (calculate-fare 12.0 4.0 true) => (throws AssertionError))))
