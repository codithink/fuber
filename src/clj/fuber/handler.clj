(ns fuber.handler
  (:require [compojure.core :refer [routes wrap-routes]]
            [fuber.layout :refer [error-page]]
            [fuber.routes.home :refer [home-routes]]
            [fuber.routes.driver :refer [driver-routes]]
            [compojure.route :as route]
            [fuber.env :refer [defaults]]
            [mount.core :as mount]
            [fuber.middleware :as middleware]))

(mount/defstate handler
  :start ((or (:init defaults) identity))
  :stop  ((or (:stop defaults) identity)))

(mount/defstate app
  :start
  (middleware/wrap-base
    (routes
     (-> #'driver-routes
         #_(wrap-routes middleware/wrap-csrf)
         (wrap-routes middleware/wrap-formats))
     (route/not-found
      (:body
       (error-page {:status 404
                    :title "page not found"}))))))
