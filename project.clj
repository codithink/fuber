(defproject fuber "0.1.0-SNAPSHOT"

  :description "FIXME: write description"
  :url "http://example.com/FIXME"

  :dependencies [[clj-time "0.14.2"]
                 [compojure "1.6.0"]
                 [cprop "0.1.11"]
                 [funcool/struct "1.2.0"]
                 [luminus-immutant "0.2.4"]
                 [luminus-nrepl "0.1.4"]
                 [luminus/ring-ttl-session "0.3.2"]
                 [markdown-clj "1.0.2"]
                 [metosin/muuntaja "0.5.0"]
                 [metosin/ring-http-response "0.9.0"]
                 [com.novemberain/monger "3.1.0"]
                 [mount "0.1.11"]
                 [midje "1.9.1"]
                 [refactor-nrepl "2.3.1"]
                 [org.clojure/clojure "1.8.0"]
                 [org.clojure/core.async "0.4.474"]
                 [org.clojure/tools.cli "0.3.5"]
                 [org.clojure/tools.logging "0.4.0"]
                 [org.webjars.bower/tether "1.4.3"]
                 [org.webjars/bootstrap "4.0.0"]
                 [org.webjars/font-awesome "5.0.6"]
                 [org.webjars/jquery "3.2.1"]
                 [ring-webjars "0.2.0"]
                 [ring/ring-core "1.6.3"]
                 [ring/ring-defaults "0.3.1"]
                 [selmer "1.11.6"]
                 [org.clojure/math.numeric-tower "0.0.4"]
                 [flake "0.4.5"]]

  :min-lein-version "2.0.0"

  :source-paths ["src/clj"]
  :test-paths ["test/clj"]
  :resource-paths ["resources"]
  :target-path "target/%s/"
  :main fuber.core

  :plugins [[lein-immutant "2.1.0"]
            [cider/cider-nrepl "0.17.0-SNAPSHOT"]]

  :profiles
  {:uberjar {:omit-source true
             :aot :all
             :uberjar-name "fuber.jar"
             :source-paths ["env/prod/clj"]
             :resource-paths ["env/prod/resources"]}

   :dev           [:profiles/common :project/dev :profiles/dev]
   :test          [:profiles/common :project/test :profiles/test]

   :project/dev  {:dependencies [[pjstadig/humane-test-output "0.8.3"]
                                 [prone "1.5.0"]
                                 [ring/ring-devel "1.6.3"]
                                 [ring/ring-mock "0.3.2"]]
                  :plugins      [[com.jakemccrary/lein-test-refresh "0.19.0"]]
                  :source-paths ["env/dev/clj"]
                  :resource-paths ["env/dev/resources"]
                  :repl-options {:init-ns fuber.core}
                  :injections     [(require 'pjstadig.humane-test-output)
                                   (pjstadig.humane-test-output/activate!)]}
   :project/test {:dependencies [[pjstadig/humane-test-output "0.8.3"]
                                 [midje "1.8.3"]]
                  :plugins      [[lein-midje "3.2.1"]]
                  :source-paths   ["env/test/clj"]
                  :resource-paths ["env/test/resources"]}})
