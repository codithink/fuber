(ns fuber.test.utils-test
  (:use  midje.sweet)
  (:require [midje.config :refer :all]
            [fuber.utils :refer :all]))

(change-defaults :print-level :print-facts)

(facts "distance between two points function"

       (fact "distance between the same point should be zero"
             (distance-between-two-points [4.0 4.0] [4.0 4.0]) => 0.0
             (distance-between-two-points [-4.0 -4.0] [-4.0 -4.0]) => 0.0)

       (fact "AssertionError should be thrown when either the longitude or the lattitude is not a float"
             (distance-between-two-points [4 4.0] [6 6.0]) => (throws AssertionError)
             (distance-between-two-points ["test" 4] [6 6]) => (throws AssertionError))

       (fact "If pythagoras formula is working"
             (distance-between-two-points [3.0 3.0] [6.0 7.0]) => 5.0
             (distance-between-two-points [13.0 3.55] [6.33 7.04]) => 7.53))

(facts "set-precision function"

       (fact "that only floating numbers can be allowed"
             (set-precision "testing") => (throws AssertionError)
             (set-precision 123) => (throws AssertionError)
             (set-precision nil) => (throws AssertionError))

       (fact "Set-precision till 2 decimal places"
             (set-precision 12.443434434 2) => 12.44
             (set-precision 12.0 5) => 12.0)

       (fact "Round off if precision parameter is not provided or is negative"
             (set-precision 123.3243) => 123
             (set-precision 323.323 -1) => 323))
