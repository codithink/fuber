(ns fuber.utils
  (:require [clojure.math.numeric-tower :as math]
            [fuber.db :refer [db]]
            [monger.collection :as mc]
            [monger.operators :refer :all]
            [clj-time.core :as t]
            [clj-time.format :as f])
  (:import org.bson.types.ObjectId))

(comment (set-precision 27.4324432 2))

(defn set-precision
  [x & [p]]
  {:pre [(and (float? x) (or (nil? p) (integer? p)))]}
  (if (and p (pos? p))
    (let [scale (Math/pow 10 p)]
      (-> x (* scale) Math/round (/ scale)))
    (Math/round x)))

(comment (distance-between-two-points [123.11 323.323] [323.32 434.33]))

(defn distance-between-two-points
  [[long-1 lat-1] [long-2 lat-2]]
  {:pre [(every? float? [long-1 lat-1 long-2 lat-2])]
   :post [float?]}
  (let [sqr (fn [x] (* x x))
        sqr-of-difference (fn [x1 x2]
                            (sqr (- x1 x2)))]
    (-> (sqr-of-difference long-2 long-1)
        (+ (sqr-of-difference lat-2 lat-1))
        math/sqrt
        math/abs
        (set-precision 2))))

(comment (update-driver "5a82f19cc07eb11879199314" {:available? false}))

(defn update-driver
  [id condition]
  {:pre [(map? condition)]}
  (mc/update db
             "drivers"
             {:_id (ObjectId. id)}
             {$set condition}))

(comment (update-driver "5a82f19cc07bht1879199314" {:available? false}))

(defn update-customer
  [id condition]
  {:pre [(map? condition)]}
  (mc/update db
             "customers"
             {:_id (ObjectId. id)}
             {$set condition}))

(comment (find-driver "5a82f19cc07eb1187919930c"))

(defn find-driver
  "Returns the details of the car"
  [id]
  (try
    (mc/find-one-as-map db "drivers"
                        {:_id (ObjectId. id)})
    (catch NullPointerException e)
    (finally nil)))

(comment (find-driver "5a82f19cc07eb1187919930e"))

(defn find-customer
  "Returns the details of the car"
  [id]
  (try
    (mc/find-one-as-map db "customers"
                        {:_id (ObjectId. id)})
    (catch NullPointerException e)
    (finally nil)))

(comment (insert-n-drivers 10))

(defn insert-n-drivers
  "Insert n drivers with random lattitudes and longitudes in the collection named drivers.

  Each driver will have four attributes (id, position ,available? and pink?)"
  [n]
  {:pre [(integer? n)]}
  (let [cars (->> (repeatedly (fn []
                                {:position (mapv #(* (rand-int 100) %)
                                                 (vector (rand) (rand)))
                                 :pink? (> (rand) 0.85)
                                 :available? (< (rand) 0.9)}))
                  (take n))]
    (mc/insert-batch db "drivers" cars)
    cars))

(comment (insert-n-customers 10))

(defn insert-n-customers
  "Insert n customers in the customers collection.
  Each customer will have his/her position and a boolean on-trip?"
  [n]
  {:pre [(integer? n)]}
  (let [customers (->> (repeatedly (fn []
                                {:position (mapv #(* (rand-int 100) %)
                                                 (vector (rand) (rand)))
                                 :on-trip? (> (rand) 0.9)}))
                  (take n))]
    (mc/insert-batch db "customers" customers)))

(comment (_id->id {:_id (ObjectId. "5a82f19cc07eb11879199314")}))

(defn _id->id
  "Converts monger ObjectId('mongerid') to 'mongerid' "
  [m]
  (condp = (type m)
    clojure.lang.PersistentArrayMap (dissoc (assoc m :id (str (:_id m))) :_id)
    clojure.lang.PersistentHashMap (dissoc (assoc m :id (str (:_id m))) :_id)
    clojure.lang.PersistentVector (mapv #(dissoc (assoc % :id (str (:_id %))) :_id) m)
    clojure.lang.LazySeq (mapv #(dissoc (assoc % :id (str (:_id %))) :_id) m)))

;; date time parsers

(def formatter (f/formatter "YYYYMMddHHmmssSSS"))

(defn db->date [db] (f/parse formatter db))

(defn date->db [date] (f/unparse formatter date))
