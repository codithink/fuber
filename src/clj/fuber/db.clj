(ns fuber.db
  (:require [monger.core :as mg]
            [mount.core :refer [defstate]]
            [fuber.config :refer [env]]))

(def conn (atom nil))

(defstate db
  :start
  (let [db-name (:db-name (:database env))
        url (str "mongodb://127.0.0.1:27017/" db-name)]
    (reset! conn (-> url mg/connect-via-uri :conn))
    (-> url mg/connect-via-uri :db))
  :stop
  (mg/disconnect @conn))
