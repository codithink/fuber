(ns fuber.test.utils
  (:require [fuber.core :refer [-main]]
            [fuber.db :refer [db]]
            [fuber.utils :refer [_id->id date->db]]
            [monger.collection :as mc]
            [monger.operators :refer :all]
            [clj-time.core :as t])
  (:import org.bson.types.ObjectId))

(defn teardown
  "Removes the collections from the database"
  []
  (run! (fn [coll]
          (mc/remove db coll))
        ["drivers" "customers" "trips"]))

(def occupied-car
  {:available? false
   :pink? false
   :current-source [24.0 25.0]
   :started-time "20180219134221759"
   :position [23.0 24.5]})

(def non-existent-car
  {:id "123123123123123123123123"})

(def available-car
  {:available? true
   :pink? false
   :position [18.5 17.5]})

(def available-car-2
  {:available? true
   :pink? false
   :position [24.5 23.5]})

(def available-car-3
  {:available? true
   :pink? false
   :position [18.5 17.5]})

(def available-car-4
  {:available? true
   :pink? false
   :position [18.5 188.5]})

(def available-car-5
  {:available? true
   :pink? false
   :position [25.22 13.43]})

(def occupied-pink-car
  {:available? false
   :pink? true
   :started-time "20180219134221759"
   :current-source [24.0 25.0]
   :position [23.0 24.5]})

(def available-pink-car
  {:available? true
   :pink? true
   :position [18.5 17.5]})

(def available-pink-car-2
  {:available? true
   :pink? true
   :position [18.5 17.5]})

(def car-not-on-trip
  {:available? true
   :pink? false
   :position [18.5 17.5]})

(def available-customer
  {:on-trip? false})

(def available-customer-2
  {:on-trip? false})

(def available-customer-3
  {:on-trip? false})

(def available-customer-4
  {:on-trip? false})

(def available-customer-5
  {:on-trip? false})

(def available-customer-6
  {:on-trip? false})

(def on-trip-customer
  {:on-trip? true})

(def customer-not-on-trip
  {:on-trip? false})

(def test-data (atom {:source-1 {:position [244.9 1723.323]}
                      :source-2 {:position [24.9 25.323]}
                      :destination-1 {:positon [38.0 38.0]}
                      :destination-2 {:positon [12.0 37.0]}}))

(defn change-source-time
  "Will change the started-time to the time 1.5 minutes ago"
  [id]
  (mc/update db
             "trips"
             {:_id (ObjectId. id)}
             {$set {:started-time (date->db (t/ago (t/seconds (* 1.5 60))))}}))

(defn insert-driver [data]
  (_id->id (mc/insert-and-return db "drivers" data)))

(defn insert-customer [data]
  (_id->id (mc/insert-and-return db "customers" data)))

(defn load-trips-data
  []
  (let [normal-trip (-> (mc/find-one-as-map db
                                       "trips"
                                       {:driver-id (:id
                                                    (:available-car @test-data))})
                        _id->id)
        pink-trip (-> (mc/find-one-as-map db
                                          "trips"
                                          {:driver-id (:id
                                                       (:available-pink-car @test-data))})
                      _id->id)]
    (swap! test-data assoc
           :invalid-trip-id "123123123123123123123123"
           :normal-trip normal-trip
           :pink-trip pink-trip)))

(defn setup
  "Will create some sample drivers and customers"
  []
  (-main)
  (teardown)
  (swap! test-data assoc
         :occupied-car (insert-driver occupied-car)
         :available-car (insert-driver available-car)
         :available-car-2 (insert-driver available-car-2)
         :available-car-3 (insert-driver available-car-3)
         :available-car-4 (insert-driver available-car-4)
         :available-car-5 (insert-driver available-car-5)
         :available-pink-car (insert-driver available-pink-car)
         :available-pink-car-2 (insert-driver available-pink-car)
         :occupied-pink-car (insert-driver occupied-pink-car)
         :non-existent-car non-existent-car
         :available-customer (insert-customer available-customer)
         :available-customer-2 (insert-customer available-customer-2)
         :available-customer-3 (insert-customer available-customer-3)
         :available-customer-4 (insert-customer available-customer-4)
         :available-customer-5 (insert-customer available-customer-5)
         :available-customer-6 (insert-customer available-customer-6)
         :on-trip-customer (insert-customer on-trip-customer)
         :customer-not-on-trip (insert-customer customer-not-on-trip)
         :car-not-on-trip (insert-driver car-not-on-trip)))
