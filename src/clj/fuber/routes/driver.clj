(ns fuber.routes.driver
  (:require [compojure.core :refer [defroutes GET POST PATCH context]]
            [fuber.driver :as d]
            [fuber.routes.route-utils :refer [send-error-response
                                              send-success-response]]))

(defn- str-to-num
  "Helper function to convert numbers in string to the original format

  Will return the original number if the data is not a string"
  [arr]
  (map (fn [x]
         (if (string? x)
           (read-string x)
           x))
       arr))

(defn get-nearest-driver
  "Helper function for get-nearest-car"
  [cust-pos-lat cust-pos-long pink?]
  (if-not (some nil? [cust-pos-lat cust-pos-long pink?])
    (let [res (d/get-nearest-driver (str-to-num [cust-pos-lat cust-pos-long])
                                    pink?)]
      (if (= "No cars nearby" res)
        (send-error-response "No cars nearby")
        (send-success-response res)))
    (send-error-response "Invalid parameters are passed.")))

(defn start-car-trip
  "Helper function for car-trip fn"
  [driver-id cust-id source destination]
  (if-not (some nil? [driver-id cust-id source destination])
    (let [res (d/start-car-trip driver-id
                                cust-id
                                (str-to-num source)
                                (str-to-num destination))]
      (if (:trip-started? res)
        (send-success-response res)
        (send-error-response res)))
    (send-error-response "Invalid parameters are passed.")))

(defn end-car-trip
  "Helper function for car-trip fn"
  [id destination]
  (if-not (some nil? [id destination])
    (let [res (d/end-car-trip id (str-to-num destination))]
      (if (:trip-finished? res)
        (send-success-response res)
        (send-error-response res)))
    (send-error-response "Invalid parameters are passed.")))

(defn driver-trips [id]
  (if-not (nil? id)
    (let [res (d/booking-history id "driver")]
      (send-success-response res))
    (send-error-response "Driver ID is not present.")))

(defn customer-trips [id]
  (if-not (nil? id)
    (let [res (d/booking-history id "customer")]
      (send-success-response res))
    (send-error-response "Customer ID not present.")))

(defroutes driver-routes
  (GET "/driver/nearest" [cust-pos-lat cust-pos-long pink?]
       (get-nearest-driver cust-pos-lat cust-pos-long pink?))
  (POST "/trip" [driver-id cust-id source destination]
        (start-car-trip driver-id cust-id source destination))
  (PATCH "/trip" [id destination]
        (end-car-trip id destination))
  (GET "/trips/driver" [id]
       (driver-trips id))
  (GET "/trips/customer" [id]
       (customer-trips id)))
