(ns fuber.test.test-core
  (:use  midje.sweet)
  (:require [midje.config :refer :all]))

(change-defaults :print-level :print-facts)

(fact "Sample test"
      (+ 2 2)
      => 4)
