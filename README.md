# fuber

You are the proprietor of füber, an on call taxi service.

You have a fleet of cabs at your disposal, and each cab has a
location, determined by it's latitude and longitude.
A customer can call one of your taxis by providing their location, and
you must assign the nearest taxi to the customer.

Some customers are particular that they only ride around in pink cars,
for hipster reasons. You must support this ability.
When the cab is assigned to the customer, it can no longer pick up any
other customers.
If there are no taxis available, you reject the customers request.

The customer ends the ride at some location. The cab waits around
outside the customer's house, and is available to be assigned to
another customer.

Notes:

We expect good unit tests.
Unfortunately, you skipped Geography, and believe the earth is flat.
The distance between two points can be calculated by Pythagoras'
theorem.
We don't expect a front end for this, but try to build an restful API.

Extra Credit:

When the customer ends the ride, log the total amount the customer owes.
The price is 1 dogecoin per minute, and 2 dogecoin per kilometer. Pink
cars cost an additional 5 dogecoin.
HTML front end showing where all the cars available.


## API's:

1. GET "/driver/nearest" :
- The API will take the customer-position(lattitude and longitude in seperate parameters) and return the nearest driver within 5 kms of the position.
- The API will return the driver-id of the closest driver.
- If there are multiple drivers, then the closest driver will be returned.
- If there are no drivers, it will return "No cars nearby"

2. POST "/trip":

- This API will use the driver-id fetched from the above API call to call this API.
- The API will create a new trip with the given driver-id and cust-id. A new document will be added in the trips collection in the database.
- The trip's id will be updated in the related driver and the customer.
- The trip has a attribute on-trip? which indicates the status of the trip.
- The API will return estimate fare and duration values of the trip based on the destination value.
- The destination parameter is used only for giving the estimate values in this API.

3. PATCH "/trip"

- This API is to end the trip.
   - set on-trip to false
   - customer and drivers will be available for other trips.
- The API will return the fare of the trip, distance covered and duration of the trip.
- The trip-id will be removed from the customer and the driver's documents.

4. GET "/trips/driver"

- The API will return all the trips the driver has been associated with.

5. GET "/trips/customer"

- The API will return all the trips the customer has been associated with.

## Notes:


1. The database consists of three collections:

- drivers:
   - A driver row will consist of:
	   - _id - unique id
	   - position - array of lattitude and longitude
       - available? - availability of driver
	   - pink? - boolean value of whether the car is pink or not

- customers:
   - A customer row will consist of:
	   - _id - unique id
	   - position - array of lattitude and longitude
	   - on-trip? - boolean value

- trips:
   - A trip row will consist of:
	   - _id - unique id
       - on-trip? - boolean
       - driver-id - driver's unique id
       - cust-id - customer's unique id
       - source - position of trip source
       - destination - position of trip destination
       - started-time - time at which trip started
       - end-time - time at which trip ended
       - distance - distance covered in the trip
       - pink? - boolean value
       - fare - fare of the trip.


## Steps to setup http-server,database and n-repl server

1. Do ```lein repl``` to start the repl of this project.
2. By default, the ```fuber.core``` namespace should be loaded.If not, navigate to the core namespace.
3. run the command ```(-main)``` in the repl prompt.


## Steps to setup database:

1. Install mongo in your system.
2. Setup the mongo server by the command ```mongod``` in the terminal and make sure that it is running in its default port(27017).
3. In another terminal, open the mongo client with the command ```mongo```.
4. Create two databases with names as fuber and fuber-test.Database in mongo can be created by the ```use``` command.(Eg: use fuber).

### Add sample data in database:

1. Start the repl and go to the namespace fuber.utils.
2. The function insert-n-drivers will insert n drivers in the drivers collection.
3. No need to do anything with the test db, data will automatically be inserted when running the tests.

## Steps to run tests

1. Use the command ```lein with-profile +test midje``` to run all the tests.
2. The above command will connect to the ```fuber-test``` db and will quit after all the tests are run.
3. To run the tests continuosly, do add ```:autotest``` to the 1st step's command.
