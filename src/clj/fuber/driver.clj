(ns fuber.driver
  (:require [fuber.utils :as u]
            [flake.core :as flake]
            [flake.utils :as flake-utils]
            [fuber.db :refer [db]]
            [monger.collection :as mc]
            [monger.operators :refer :all]
            [clj-time.core :as t])
  (:import org.bson.types.ObjectId))

(defonce car-speed 20)

(comment (get-nearest-driver [18.0 25.3] true)) ;; For pink cars

(comment (get-nearest-driver [18.0 25.3] false)) ;; Normal cars

(def driver-object (Object.))

(defn get-nearest-driver
  "Returns the nearest available car within 5 kms to the driver's position
  People who prefer pink color cars will be fetched pink colour cars only
  and for other people non-pink cars will be returned"
  [d-position pink?]
  {:pre [(and (coll? d-position) (= 2 (count d-position)))]}
  (let [cars-in-range (->> (mc/find-maps db "drivers")
                           (filter (fn [car]
                                     (and
                                      (< (u/distance-between-two-points
                                          (:position car)
                                          d-position)
                                         5)
                                      (:available? car)
                                      (= pink? (:pink? car))))))]
    (condp = (count cars-in-range)
      0 "No cars nearby"
      1 (u/_id->id (first cars-in-range))
      (->> cars-in-range
           (sort-by (fn [car]
                      (u/distance-between-two-points d-position
                                                     (:position car))))
           first
           u/_id->id))))

(comment (check-car-availability "5a82f19cc07eb1187919930c"))

(defn check-car-availability
  "Given a car-id will update the availability of the car
  according to the current availability"
  [id]
  (let [current-car (u/find-driver id)]
    (cond
      (nil? current-car)
      "No such car exists."
      (= (:available? current-car)
         false)
      "Car is already occupied"
      (:available? current-car)
      "available")))

(comment (calculate-fare 12.33 2))

(defn calculate-fare
  "Calculates the fare of the trip given the source,destination and the speed
  For normal cars 1 dodgecoin per minute and 2 dodgecoins per km
  For pink cars additional 5 dodgecoins
  The price is returned accurate to 2 decimal places"
  [distance time-in-mins pink?]
  {:pre [(and (float? distance)
              (pos? distance))
         (and (integer? time-in-mins)
              (pos? time-in-mins))]}
  (let [normal-fare (+ time-in-mins (* 2 distance))]
    (-> (if pink?
          (+ normal-fare 5)
          normal-fare)
        (u/set-precision 2))))

(comment (start-car-trip "5a82f19cc07eb1187919930c" [12.3 14.5]))

(defn h-start-car-trip
  "A trip will start if the car is available.
  The trip details will be added in the trips collection.
  The current-trip id will be added in the driver's and the customer's
  document in the database.
  The function will return estimate values of fare and duration.
  While on trip, the car will not be availble for other customers"
  [driver-id cust-id source destination]
  (let [current-car (u/find-driver driver-id)
        car-status (check-car-availability driver-id)
        customer (mc/find-one-as-map db "customers" {:_id (ObjectId. cust-id)})
        h-start-trip (fn  [driver-id cust-id source destination current-car]
                       (let [estimate-distance (u/distance-between-two-points source
                                                                              destination)
                             estimate-time (/ estimate-distance car-speed)
                             estimate-time-mins (u/set-precision (* estimate-time 60))
                             estimate-fare (calculate-fare estimate-distance
                                                           estimate-time-mins
                                                           (:pink? current-car))
                             trip-id (-> (mc/insert-and-return db
                                                            "trips"
                                                            {:on-trip? true
                                                             :driver-id driver-id
                                                             :cust-id cust-id
                                                             :source source
                                                             :started-time (u/date->db (t/now))
                                                             :pink? (:pink? current-car)})
                                      u/_id->id
                                      :id)]
                         (u/update-driver driver-id {:available? false
                                                     :current-trip trip-id})
                         (u/update-customer cust-id {:on-trip? true
                                                     :current-trip trip-id})
                         {:trip-started? true
                          :trip-id trip-id
                          :source source
                          :estimate-distance estimate-distance
                          :destination destination
                          :estimate-time-mins estimate-time-mins
                          :estimate-fare estimate-fare}))]

    (cond

      (nil? current-car)
      {:trip-started? false
       :error-message car-status}

      (nil? customer)
      {:trip-started? false
       :error-message "No customer with the given id exists in the database"}

      (not= car-status "available")
      {:trip-started? false
       :error-message car-status}

      (:on-trip? customer)
      {:trip-started? false
       :error-message "A customer can book only one trip at a time"}

      (> (u/distance-between-two-points (:position current-car)
                                         source)
         5)
      {:trip-started? false
       :error-message "Driver is too far away"}

      :else
      (h-start-trip driver-id cust-id source destination current-car))))

(defn start-car-trip
  [driver-id cust-id source destination]
  (locking driver-object
    (h-start-car-trip driver-id cust-id source destination)))

(comment (end-car-trip "5a82f19cc07eb1187919930c" "5a82f19cc07eb1187919930d" [18.3 24.5]))

(defn end-car-trip
  "The trip will end of the trip's id.
  The customer and driver will be able to attend other trips after that.
  On failure of ending a trip, the trip-finished? will be false
  with a error-message.
  On successful ending of a trip, the fare,distance and trip-duration is
  returned.
  The distance is calculated from where the customer
  has boarded to the destination provided.
  The trip details will be updated in the trip collection."
  [trip-id destination]
  {:pre [(not (nil? trip-id)) (not (nil? destination))]}
  (let [trip (some-> (mc/find-one-as-map db
                                         "trips"
                                         {:_id (ObjectId. trip-id)})
                     u/_id->id )
        driver-id (:driver-id trip)
        cust-id (:cust-id trip)
        customer (u/find-customer cust-id)
        h-end-trip (fn [driver-id cust-id destination pink?]
                     (let [trip-source (:source trip)
                           distance (u/distance-between-two-points trip-source
                                                                   destination)
                           trip-end-time (t/now)
                           time-in-sec (-> (:started-time trip)
                                           (u/db->date)
                                           (t/interval trip-end-time)
                                           (t/in-seconds))
                           time-in-mins (int (/ time-in-sec 60))
                           trip-fare (calculate-fare distance
                                                     time-in-mins
                                                     pink?)]
                       (u/update-driver driver-id {:position destination
                                                   :available? true})
                       (u/update-customer cust-id {:position destination
                                                   :on-trip? false})
                       ;; Remove current trip-id
                       (mc/update db "drivers" {:_id (ObjectId. driver-id)}
                                  {$unset {:current-trip 1}})
                       (mc/update db "customers" {:_id (ObjectId. cust-id)}
                                  {$unset {:current-trip 1}})
                       (mc/update db "trips"
                                  {:_id (ObjectId. trip-id)}
                                  {$set {:on-trip? false
                                         :fare trip-fare
                                         :destination destination
                                         :distance distance
                                         :end-time (u/date->db trip-end-time)}})
                       {:trip-finished? true
                        :distance distance
                        :fare trip-fare
                        :trip-duration time-in-mins}))]


    (cond

      (nil? trip)
      {:trip-finished? false
       :error-message "Trip-id does not exist in the database."}

      (false? (:on-trip? trip))
      {:trip-finished? false
       :error-message "The trip has already finished."}

      :else
      (h-end-trip driver-id cust-id destination (:pink? trip)))))

(defn booking-history
  "All the trips in which the person related to the ID will be returned.
  To differentiate between customer and driver, the who key is used."
  [id who]
  {:pre [(or (= who "driver")
             (= who "customer")) (not (nil? id))]}
  (let [who-key (if (= who "driver")
                  :driver-id
                  :cust-id)]
    (some->> (mc/find-maps db "trips" {who-key id
                                       :on-trip? false})
             (sort-by #(compare %2 %1) :start-time)
             (map u/_id->id))))
